====== FAQ: I Forgot my Password ======
The lab administrator can not retrieve your password for you. He will have you create a new password for your self or set a temporary password for you to login with and change your own password. Please contact the lab administrator if you forgot your password at[[Linuxlabhelp@my.harrisburgu.edu|mailto:Linuxlabhelp@my.harrisburgu.edu]].

Please see [[faq:password_change|FAQ: How to Change Your Password in the Lab]] to change your password after receiving your temporay password.